package logger

type Option struct {
	Level string
}

/**
 * create binder interface
 */
type Logger interface {
	// ----
	IsDebugEnabled() bool

	IsInfoEnabled() bool

	IsWarnEnabled() bool

	IsErrorEnabled() bool

	Debug(msg string)

	Info(msg string)

	Warn(msg string)

	Error(msg string)
}

/**
 * define logger factory handle
 */
type LoggerFactory interface {
	GetLogger(opts ...Option) Logger
}
